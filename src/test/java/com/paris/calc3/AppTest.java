package com.paris.calc3;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void testApp()
    {
        assertEquals(7d,new Calculator().calculate("plus","3","4"),0.0001 );
    }
}
